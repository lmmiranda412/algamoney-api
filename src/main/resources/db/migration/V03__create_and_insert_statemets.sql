CREATE TABLE statement (
                           id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
                           description VARCHAR(50) NOT NULL,
                           dueDate DATE NOT NULL,
                           paymentDate DATE,
                           amount DECIMAL(10,2) NOT NULL,
                           note VARCHAR(100),
                           type VARCHAR(20) NOT NULL,
                           categoryId BIGINT(20) NOT NULL,
                           personId BIGINT(20) NOT NULL,
                           FOREIGN KEY (categoryId) REFERENCES category(id),
                           FOREIGN KEY (personId) REFERENCES person(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Salário mensal', '2017-06-10', null, 6500.00, 'Distribuição de lucros', 'INCOME', 1, 1);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Bahamas', '2017-02-10', '2017-02-10', 100.32, null, 'EXPENSE', 2, 2);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Top Club', '2017-06-10', null, 120, null, 'INCOME', 3, 3);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('CEMIG', '2017-02-10', '2017-02-10', 110.44, 'Geração', 'INCOME', 3, 4);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('DMAE', '2017-06-10', null, 200.30, null, 'EXPENSE', 3, 5);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Extra', '2017-03-10', '2017-03-10', 1010.32, null, 'INCOME', 4, 6);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Bahamas', '2017-06-10', null, 500, null, 'INCOME', 1, 7);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Top Club', '2017-03-10', '2017-03-10', 400.32, null, 'EXPENSE', 4, 8);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Despachante', '2017-06-10', null, 123.64, 'Multas', 'EXPENSE', 3, 9);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Pneus', '2017-04-10', '2017-04-10', 665.33, null, 'INCOME', 5, 10);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Café', '2017-06-10', null, 8.32, null, 'EXPENSE', 1, 5);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Eletrônicos', '2017-04-10', '2017-04-10', 2100.32, null, 'EXPENSE', 5, 4);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Instrumentos', '2017-06-10', null, 1040.32, null, 'EXPENSE', 4, 3);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Café', '2017-04-10', '2017-04-10', 4.32, null, 'EXPENSE', 4, 2);
INSERT INTO statement (description, dueDate, paymentDate, amount, note, type, categoryId, personId) values ('Lanche', '2017-06-10', null, 10.20, null, 'EXPENSE', 4, 1);