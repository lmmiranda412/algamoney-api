package com.algaworks.algamoneyapi.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ControllerAdvice
public class AlgamoneyExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Error error = new Error();
        error.setMessage(messageSource.getMessage("object-not-readable", null, LocaleContextHolder.getLocale()));
        String cause = Optional.ofNullable(ex.getCause()).orElse(ex).toString();
        error.setCause(cause);

        return handleExceptionInternal(ex, Arrays.asList(error), headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<Error> errors = handleErrors(ex.getBindingResult());
        return handleExceptionInternal(ex, errors, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Object> handleResponseStatusException(ResponseStatusException ex, WebRequest request) {
        Error error = new Error();
        error.setMessage(messageSource.getMessage(ex.getReason(), null, LocaleContextHolder.getLocale()));
        error.setCause(ex.getMessage());

        return handleExceptionInternal(ex, Arrays.asList(error), new HttpHeaders(), ex.getStatus(), request);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        Error error = new Error();
        error.setMessage(messageSource.getMessage("resource.not-found", null, LocaleContextHolder.getLocale()));
        error.setCause(ex.getMessage());

        return handleExceptionInternal(ex, Arrays.asList(error), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    private List<Error> handleErrors(BindingResult bindingResult) {
        List<Error> errors = new ArrayList<>();

        bindingResult.getFieldErrors().forEach(fieldError -> {
            Error error = new Error();

            error.setMessage(messageSource.getMessage(fieldError, LocaleContextHolder.getLocale()));
            error.setCause(fieldError.toString());

            errors.add(error);
        });

        return errors;
    }
}
