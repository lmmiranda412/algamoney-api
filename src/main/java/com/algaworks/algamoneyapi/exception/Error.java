package com.algaworks.algamoneyapi.exception;

import lombok.Data;

@Data
public class Error {

    private String message;

    private String cause;

}
