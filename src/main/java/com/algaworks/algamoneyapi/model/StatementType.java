package com.algaworks.algamoneyapi.model;


public enum StatementType {
    INCOME,
    EXPENSE
}
