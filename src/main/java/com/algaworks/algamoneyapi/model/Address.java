package com.algaworks.algamoneyapi.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class Address {

    private String address;

    private Integer number;

    private String complement;

    private String district;

    private String zipCode;

    private String city;

    private String state;

}
