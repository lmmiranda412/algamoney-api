package com.algaworks.algamoneyapi.controller;

import com.algaworks.algamoneyapi.model.Statement;
import com.algaworks.algamoneyapi.service.StatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/statements")
public class StatementController {

    @Autowired
    private StatementService statementService;

    @GetMapping("/{id}")
    public Statement findById(@PathVariable Long id) {
        return statementService.findById(id);
    }

    @GetMapping
    public List<Statement> findAll() {
        return statementService.findAll();
    }
}
