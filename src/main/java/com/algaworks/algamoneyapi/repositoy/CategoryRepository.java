package com.algaworks.algamoneyapi.repositoy;

import com.algaworks.algamoneyapi.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
