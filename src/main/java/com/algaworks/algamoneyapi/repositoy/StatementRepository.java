package com.algaworks.algamoneyapi.repositoy;

import com.algaworks.algamoneyapi.model.Statement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatementRepository extends JpaRepository<Statement, Long> {
}
