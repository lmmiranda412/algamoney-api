package com.algaworks.algamoneyapi.service;

import com.algaworks.algamoneyapi.exception.BusinessException;
import com.algaworks.algamoneyapi.model.Category;
import com.algaworks.algamoneyapi.repositoy.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll() {
        List<Category> categories = categoryRepository.findAll();
        return categories;
    }

    public Category create(Category category) {
        category = categoryRepository.save(category);
        return category;
    }

    public Category findById(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "resource-not-found"));
        return category;
    }
}
