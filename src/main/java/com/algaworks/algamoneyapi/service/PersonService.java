package com.algaworks.algamoneyapi.service;

import com.algaworks.algamoneyapi.exception.BusinessException;
import com.algaworks.algamoneyapi.model.Person;
import com.algaworks.algamoneyapi.repositoy.PersonRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Person create(Person person) {
        person = personRepository.save(person);
        return person;
    }

    public Person findById(Long id) {
        Person person = personRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"resource.not-found"));
        return person;
    }

    public void deleteById(Long id) {
        personRepository.deleteById(id);
    }

    public Person update(Long id, Person person) {
        Person personToUpdate = personRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "resource.not-found"));
        BeanUtils.copyProperties(person, personToUpdate, "id");
        return personRepository.save(personToUpdate);
    }

    public void updateStatus(Long id, Boolean isActive) {
        Person person = findById(id);
        person.setIsActive(isActive);
        personRepository.save(person);
    }
}
