package com.algaworks.algamoneyapi.service;

import com.algaworks.algamoneyapi.model.Statement;
import com.algaworks.algamoneyapi.repositoy.StatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class StatementService {

    @Autowired
    private StatementRepository statementRepository;

    public Statement findById(Long id) {
        Statement statement = statementRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "resource.not-found"));
        return statement;
    }

    public List<Statement> findAll() {
        List<Statement> statements = statementRepository.findAll();
        return statements;
    }

}
