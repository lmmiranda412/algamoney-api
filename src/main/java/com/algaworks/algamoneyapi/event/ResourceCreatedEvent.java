package com.algaworks.algamoneyapi.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;

@Getter
public class ResourceCreatedEvent extends ApplicationEvent {

    private HttpServletResponse httpServletResponse;

    private Long resourceId;

    public ResourceCreatedEvent(Object source, HttpServletResponse httpServletResponse, Long resourceId) {
        super(source);
        this.httpServletResponse = httpServletResponse;
        this.resourceId = resourceId;
    }
}
