package com.algaworks.algamoneyapi.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Component
public class ResourceCreatedEventListener implements ApplicationListener<ResourceCreatedEvent> {

    @Override
    public void onApplicationEvent(ResourceCreatedEvent resourceCreatedEvent) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{resourceId}")
                .buildAndExpand(resourceCreatedEvent.getResourceId()).toUri();

        resourceCreatedEvent.getHttpServletResponse().setHeader("Location", uri.toASCIIString());
    }
}
